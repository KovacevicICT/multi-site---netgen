#!/usr/bin/env bash
set -e

# RESET
if [[ -L "docker-compose.yml" && -h "docker-compose.yml" ]]
then
    docker-compose stop
fi;

echo " > Select your platform: (osx, linux, windows): ";
read PLATFORM

if [[ $PLATFORM && ( $PLATFORM == "osx" || $PLATFORM == "linux" || $PLATFORM == "windows" ) ]]
then
    echo Selected: $PLATFORM;
else
    echo " > Invalid platform, exiting...";
    exit 1;
fi;

PLATFORM_FILE="docker-compose-$PLATFORM.yml";

# create symlink for docker-compose.yml first
if [[ -L "docker-compose.yml" && -h "docker-compose.yml" ]]
then
    echo " > Symlink exists, continuing...";
else
    echo " > Creating your local copy of docker-compose.yml.";
    cp $PLATFORM_FILE docker-compose.yml;
fi;

# DEFAULTS
echo " > Creating .docker-data folder";
mkdir -p .docker-data

# DATABASE
echo " > Creating .docker-data/db";
mkdir -p .docker-data/db
echo " >     - setting ownership to local user";
if [[ $PLATFORM == "osx" ]]
then
    sudo chown -R $USER:staff .docker-data/db
else
    sudo chown -R $USER:$USER .docker-data/db
fi;

# SOLR
echo " > Creating .docker-data/solr";
mkdir -p ./.docker-data/solr
mkdir -p ./.docker-data/solr/snapshot_metadata

echo " >     - setting correct ownership ownership for solr";
if [[ $PLATFORM == "osx" ]]
then
    sudo chown -R $USER:staff .docker-data/solr
else
    sudo chown -R 8983:8983 .docker-data/solr
fi;

# REDIS
echo " > Creating .docker-data/redis";
mkdir -p ./.docker-data/redis
echo " >     - setting correct ownership ownership for redis";
if [[ $PLATFORM == "osx" ]]
then
    sudo chown -R $USER:staff .docker-data/redis
else
    sudo chown -R $USER:$USER .docker-data/redis
fi;

# ENV FILE
cp .env.dist .env

# DOCKER
echo " > Building docker images..."
docker-compose build --no-cache
echo " > Boot up docker containers in daemon mode..."

if [[ $PLATFORM == "osx" ]]
then
    docker-compose up -d
    #docker-sync start
else
    docker-compose up -d
fi;

# CLI:
echo " > Initialize database with Netgen media initial setup"
docker-compose exec web composer install

echo " Do you want to load default Netgen Media data into the database (this will wipe any data you might have!) (yes/no)";
read DATABASE_IMPORT

if [[ $DATABASE_IMPORT && ( $DATABASE_IMPORT == "yes" ) ]]
then
    echo " > Importing default netgen media database"
    docker-compose exec web bin/console ezp:ins netgen-media
else
    echo " > Skipping import of netgen media default database"
fi;

# NPM AND YARN
echo " > Running yarn/npm install and building manifest & assets";
npm install -y
yarn install -y
yarn build:dev
