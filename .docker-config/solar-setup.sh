#!/bin/sh

echo "--------- BOOTING UP SOLR CONFIGURATION: solar-setup ----------"
cd /opt/solr
mkdir -p server/ez/template
cp -R /app/vendor/ezsystems/ezplatform-solr-search-engine/lib/Resources/config/solr/* server/ez/template
cp server/solr/configsets/basic_configs/conf/{currency.xml,solrconfig.xml,stopwords.txt,synonyms.txt,elevate.xml} server/ez/template
cp server/solr/solr.xml server/ez
sed -i.bak '/<updateRequestProcessorChain name="add-unknown-fields-to-the-schema">/,/<\/updateRequestProcessorChain>/d' server/ez/template/solrconfig.xml
echo "----------- ENDED SOLR ------------"